"use strict"

import Phaser, { HEADLESS } from 'phaser'

/**
   Represent a chest game object, from which coins can collected.

   @class
   @augments Phaser.Gameobjects.Image

   @param {Phaser.Scene} scene
   @param {number} x
   @param {number} y
   @param {string} texture
   @param {string|number} frame
 */
function Chest(scene, x, y, texture, frame) {
    Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, texture, frame)
    /**
       The x coordinate of the Lizard
       @type {number}
     */
    this.x = x

    /**
       The y coordinate of the Lizard
       @type {number}
     */
    this.y = y


    this.anims.play("chest-closed")
}
Chest.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
Chest.prototype.constructor = Chest

Chest.prototype.open = function() {
    this.play("chest-open")
    //this.coins
}

// https://stackoverflow.com/questions/5222209/getter-setter-in-constructor
// or instead of defining it on prototype can defined within the constructor
// function using this instead of Chest.prototype
Object.defineProperties(Chest.prototype, {
    "coins": {
        "get": function() {
            // Even though the method defined on prototype
            // this will point to the individual chest
            // Check it by inspecting this, x and y will differ
            // for each different chests
            //console.log(this.x, this.y)
            if (this.anims.currentAnim.key === "chest-closed") {
                return Phaser.Math.Between(50, 200)
            } else {
                // Once opened, it should return 0 new coins not: NaN
                return 0
            }
        }
    }
})

export default Chest
