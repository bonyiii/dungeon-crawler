import Phaser from 'phaser'

import Game from './scenes/Game'
import GameUI from './scenes/GameUI'
import PreloadScene from './scenes/Preload'

const config = {
    type: Phaser.AUTO,
    parent: 'app',
    width: 400,
    height: 250,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: true
        },
    },
    scene: [PreloadScene, Game, GameUI],
    scale: {
        zoom: 2
    }
}

export default new Phaser.Game(config)
