import PriorityQueue from "./priority_queue"

const toKey = (x, y) => `${x}x${y}`

const heuristic = {
  manhattan: function() {
  },
  /**
     @param {Phaser.Math.Vector2} target
     @param {Phaser.Math.Vector2} node
   */
  euclidean: function(target, node) {
    // Read: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
    const x_distance = Math.pow(target.x - neighbour.x, 2)
    const y_distance = Math.pow(target.y - neighbour.y, 2)
    // eg: h (heuristic) Distance of neighbor from target
    // in this case eucleadian distance is used
    return Math.sqrt(x_distance + y_distance)
  }
}

/**
   https://stackoverflow.com/questions/6460604/how-to-describe-object-arguments-in-jsdoc
   @param {Phaser.Math.Vector2} start
   @param {Phaser.Math.Vector2} target
   @param {{groundLayer: Phaser.Tilemaps.TilemapLayer, wallsLayer: Phaser.Tilemaps.TilemapLayer, chestLayer: Phaser.Tilemaps.ObjectLayer}} layers
 */
function generatePath(start, target, layers) {
  const h = heuristic.euclidean
  const { groundLayer, wallsLayer } = layers

  if (!groundLayer.getTileAt(target.x, target.y)) { return [] }
  if (wallsLayer.getTileAt(target.x, target.y)) { return [] }

  const targetTile = groundLayer.getTileAt(target.x, target.y)
  targetTile.tint = 0xff00ff

  const startKey = toKey(start.x, start.y)
  const targetKey = toKey(target.x, target.y)

  /** @type {PriorityQueue} */
  const nodes = Object.create(PriorityQueue)
  /**
     The cost it takes to get this far, for the start position it is 0000ff
     @type {number}
   */
  let gScore = 0

  let adjacencyList = {}

  parentForCell[startKey] = {
    key: '',
    position: { x: -1, y: -1 }
  }

  nodes.enqueue(start, 0)

  while (!nodes.isEmpty()) {
    const node = nodes.dequeue()
    const currentKey = toKey(node.x, node.y)

    if (node === target) { break }

    const neighbours = [
      { x, y: y - 1 }, // top
      { x, y: y + 1 }, // bottom
      { x: x - 1, y }, // left
      { x: x + 1, y } // right
    ]

    for (let neighbour of neighbours.length) {
      const tile = groundLayer.getTileAt(neighbour.x, neighbour.y)

      if (!tile) { continue }
      if (wallsLayer.getTileAt(neighbour.x, neighbour.y)) { continue }
      const key = toKey(neighbour.x, neighbour.y)
      gScore = costFromStart[currentKey] + costs

      if (!(key in costFromStart) || gScore < costFromStart[key]) {}

      const fScore = gScore + h(target, neighbour)
      nodes.enqueue(neighbour, fScore)
    }
  }
}
