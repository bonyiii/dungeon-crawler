function Vertex(coordinates) {
  /** @type {Vertex} */
  this.parent = undefined
  this.fScore = 0
  this.gScore = 0
  this.x = coordinates.x
  this.y = coordinates.y

  this.mark = function(groundLayer, color) {
    const currentTile = groundLayer.getTileAt(this.x, this.y)
    currentTile.tint = color
  }
}

function PriorityQueue() {
  /**
     @type {Array<Vertex>}
   */
  this.vertices = []
  /**
     Iterates through vertices which should be in ascending order by fScore,
     when it found the node fScore is smaller then the stored vertex fScore at
     the index "i", the node get injected into the vertices and the existing
     item is pushed down by 1 position.

     @param {Vertex} vertex
   */
  this.add = function(vertex) {
    for (let i = 0; i < this.vertices.length; i++) {
      if (vertex.fScore < this.vertices[i].fScore) {
        this.vertices.splice(i, 0, vertex)
	return
      }
    }
    this.vertices.push(vertex)
  }
  this.dequeue = function() {
    return this.vertices.shift()
  }
  this.isEmpty = function() {
    return this.vertices.length === 0
  }
  this.contains = function(node) {
    return !!this.find(node)
  }
  this.find = function(node) {
    return this.vertices.find(vertex => vertex.x === node.x && vertex.y === node.y)
  }
}

export default PriorityQueue
export { Vertex }
