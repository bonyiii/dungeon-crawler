import { waitForSeconds } from "./debug"

const toKey = (x, y) => `${x}x${y}`

/**
   @param {Phaser.Math.Vector2} start
   @param {Phaser.Math.Vector2} target
   @param {{
   groundLayer: Phaser.Tilemaps.TilemapLayer,
   wallslayer: Phaser.Tilemaps.TilemapLayer,
   chestLayer:Phaser.Tilemaps.Objectlayer}} layers
 */
async function findPath(start, target, layers) {
  /**
     @type {Phaser.Tilemaps.TilemapLayer} groundLayer
     @type {Phaser.Tilemaps.TilemapLayer} wallsLayer
     @type {Phaser.Tilemaps.ObjectLayer} chestLayer
   */
  const { groundLayer, wallsLayer } = layers
  // console.log(start, target, groundLayer.getTileAt(target.x, target.y))
  if (!groundLayer.getTileAt(target.x, target.y)) { return [] }
  if (wallsLayer.getTileAt(target.x, target.y)) { return [] }

  const targetTile = groundLayer.getTileAt(target.x, target.y)
  // purpleish
  targetTile.tint = 0xff00ff

  const queue = []

  /**
     @typedef Position
     @property {number} x
     @property {number} y
   */

  /**
     @typedef {Object} Cell
     @property {string} key
     @property {Position} position
   */
  /**
     @type {Object<string, Cell}
   */
  // or maybe this type def would work but it's strange @type {Object<string, Object<string, Object<number>>>}
  const parentForKey = {}

  const startKey = toKey(start.x, start.y)
  const targetKey = toKey(target.x, target.y)

  parentForKey[startKey] = {
    key: '',
    position: { x: -1, y: -1 }
  }

  queue.push(start)

  while (queue.length > 0) {
    const { x, y } = queue.shift()
    const currentKey = toKey(x, y)
    if (currentKey === targetKey) { break }
    if (currentKey !== startKey) {
      const ctile = groundLayer.getTileAt(x, y)
      ctile.tint = 0x0000ff
    }

    const neighbours = [
      { x: x, y: y - 1 }, // top
      { x: x, y: y + 1 }, // bottom
      { x: x - 1, y: y }, // left
      { x: x + 1, y: y } // right
    ]

    for (let i = 0; i < neighbours.length; i++) {
      const neighbour = neighbours[i]
      const tile = groundLayer.getTileAt(neighbour.x, neighbour.y)

      if (!tile) { continue }
      if (wallsLayer.getTileAt(neighbour.x, neighbour.y)) { continue }

      // light blue -- https://htmlcolorcodes.com/colors/shades-of-blue/
      // tile.tint = 0xADD8E6

      const key = toKey(neighbour.x, neighbour.y)
      if (key in parentForKey) { continue }
      parentForKey[key] = {
        key: currentKey,
        position: { x, y }
      }

      queue.push(neighbour)
      //  await waitForSeconds(0.01)
    }
  }

  // console.log(parentForKey, Object.keys(parentForKey).length)

  /** @type {Phaser.Math.Vector2} */
  const path = []

  let currentKey = targetKey
  let currentPos = parentForKey[targetKey].position

  while (true) {
    const pos = groundLayer.tileToWorldXY(currentPos.x, currentPos.y)
    // console.log(currentPos, startKey, targetKey)
    pos.x += groundLayer.tilemap.tileWidth * 0.5
    pos.y += groundLayer.tilemap.tileHeight * 0.5

    path.push(pos)
    if (currentKey !== startKey) { break }

    const { key, position } = parentForKey[currentKey]
    currentKey = key
    currentPos = position
  }

  printPath(path, groundLayer)

  // array reverse is an in place operation!
  return path.reverse()
}

function printPath(path, groundLayer) {
  path.forEach(cell => {
    const tile = groundLayer.getTileAtWorldXY(cell.x, cell.y)
    // redish
    tile.tint = 0xff0000
  })
}

export default findPath
