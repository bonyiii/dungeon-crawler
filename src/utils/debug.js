import Phaser from 'phaser'

function debugDraw(layer, scene) {
  const debugGraphics = scene.add.graphics().setAlpha(0.7)
  layer.renderDebug(debugGraphics, {
    tileColor: null,
    collidingTileColor: new Phaser.Display.Color(243, 234, 48, 255),
    faceColor: new Phaser.Display.Color(40, 39, 37, 255)
  })
}

const waitForSeconds = (secs) => {
  return new Promise(resolve => {
    setTimeout(resolve, secs * 1000)
  })
}

const resetTileColor = async (groundLayer) => {
  const defaultTileColor = 16777215 // groundLayer.getTileAt(start.x, start.y).tint
  /**
     Reset tiles color
   */
  groundLayer.forEachTile(function(tile) { tile.tint = defaultTileColor })
  await waitForSeconds(0.01)
}

export {
  debugDraw,
  resetTileColor,
  waitForSeconds
}
