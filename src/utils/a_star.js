// https://gamedevacademy.org/how-to-use-pathfinding-in-phaser/
// https://www.dynetisgames.com/2018/03/06/pathfinding-easystar-phaser-3/
// https://jsfiddle.net/juwalbose/pu0gt7nc/
// https://github.com/CodingTrain/AStar/blob/master/astarpathfinder.js
// https://github.com/Jerenaux/pathfinding_tutorial
// https://www.youtube.com/watch?v=K8asVZmpek8
import PriorityQueue, { Vertex } from './priority_queue'
import Phaser from 'phaser'
import { resetTileColor, waitForSeconds } from './debug'

/**
   Estimated cost between two points.
   Here, multiple approaches of cost calculations are implemented.
 */
const heuristic = {
  /**
     @param {Phaser.Math.Vector2|Vertex} pointA
     @param {Phaser.Math.Vector2|Vertex} pointB
   */
  euclidean: function(pointA, pointB) {
    // Read: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
    const xDistance = Math.pow(pointA.x - pointB.x, 2)
    const yDistance = Math.pow(pointA.y - pointB.y, 2)
    // eg: h (heuristic) Distance of neighbor from pointA
    // in this case eucleadian distance is used
    return Math.sqrt(xDistance + yDistance)
  },
  chebyshev: function(pointA, pointB) {
    return Phaser.Math.Distance.Chebyshev(pointA.x, pointA.y, pointB.x, pointB.y)
  },
  // https://github.com/bgrins/javascript-astar/blob/master/astar.js
  diagonal: function(pointA, pointB) {
    const D = 1
    const D2 = Math.sqrt(2)
    const d1 = Math.abs(pointA.x - pointB.x)
    const d2 = Math.abs(pointA.y - pointB.y)
    return (D * (d1 + d2)) + ((D2 - (2 * D)) * Math.min(d1, d2))
  },
  manhattan: function(pointA, pointB) {
    const d1 = Math.abs(pointA.x - pointB.x)
    const d2 = Math.abs(pointA.y - pointB.y)
    return d1 + d2
  }
}

/**
   Build a path that the character will traverse.

   We go through each node parent and build a path alongside.
   The original start position doesn't have parent so it won't
   be part of the path but the character already there so it's ok.

   The "path" param is reference to an array, defined in the invoking
   context, when a route found it gets populated and the function return
   a boolean true value.

   @param {Vertex} current
   @param {Vertex} target
   @param {Array<{x: number, y: number}>} path
   @param {Phaser.Tilemaps.TilemapLayer} groundLayer
 */
function buildPath(current, target, path, groundLayer) {
  if (current.x === target.x && current.y === target.y) {
    let temp = current
    console.log('Path')
    while (temp) {
      console.log(temp)
      if (!(temp.x === target.x && temp.y === target.y)) {
        temp.mark(groundLayer, 0x19748D)
      }
      const pos = groundLayer.tileToWorldXY(temp.x, temp.y)
      pos.x += groundLayer.tilemap.tileWidth * 0.5
      pos.y += groundLayer.tilemap.tileHeight * 0.5
      path.push(pos)
      temp = temp.parent
    }
    console.log('bingo', path)
    return true
  }
  return false
}

function isProcessed(processed, neighbour) {
  return processed.find((vertex) => {
    return vertex.x === neighbour.x && vertex.y === neighbour.y
  })
}

/**
   @param {Phaser.Tilemaps.TilemapLayer} wallsLayer
   @param{Vertex} neighbour
*/
function isWall(wallsLayer, neighbour) {
  const wallTile = wallsLayer.getTileAt(neighbour.x, neighbour.y)
  if (wallTile && wallTile.collides) { return true }
}

/**
   https://stackoverflow.com/questions/6460604/how-to-describe-object-arguments-in-jsdoc
   @param {Phaser.Math.Vector2} startVec
   @param {Phaser.Math.Vector2} targetVec
   @param {{groundLayer: Phaser.Tilemaps.TilemapLayer,
   wallsLayer: Phaser.Tilemaps.TilemapLayer,
   chestLayer: Phaser.Tilemaps.ObjectLayer}} layers
 */
async function generatePath(startVec, targetVec, layers) {
  /**
     By default we use the eucleadian heuristic but feel free to override it to
     any other available heuristic.
   */
  const h = heuristic.euclidean
  const { groundLayer, wallsLayer } = layers

  await resetTileColor(groundLayer)

  /**
     queue: is the openSet all items which needs to be evaluated are added to it.
     The algorithm can finish two ways:
     - By reaching target node, meaning that path found
     - openSet becomes empty, meanning that no path found
     @type {PriorityQueue} */
  const queue = new PriorityQueue()
  const start = new Vertex(startVec)
  const target = new Vertex(targetVec)
  target.mark(groundLayer, 0xff00ff)

  /**
     processed: items which don't need to be evaluated ever again.
     This is basically a synonim for closed set.
   */
  const processed = []
  /**
     The path what character will receive and traverse.
     @type {Array<{x: number, y: number}>}
   */
  const path = []
  queue.add(start)

  while (!queue.isEmpty()) {
    const current = queue.dequeue()
    if (buildPath(current, target, path, groundLayer)) {
      console.log('Solution found!')
      return path.reverse()
    }

    processed.push(current)
    if (!(current.x === target.x && current.y === target.y)) {
      current.mark(groundLayer, 0x993333)
    }

    // Return neighbouring tiles
    const neighbours = [
      // Horizontal/Vertical moves
      { x: current.x, y: current.y - 1 }, // top
      { x: current.x, y: current.y + 1 }, // bottom
      { x: current.x - 1, y: current.y }, // left
      { x: current.x + 1, y: current.y }, // right
      // Diagonal moves
      { x: current.x - 1, y: current.y - 1 }, // top-left
      { x: current.x + 1, y: current.y - 1 }, // top-right
      { x: current.x - 1, y: current.y + 1 }, // bottom-left
      { x: current.x + 1, y: current.y + 1 }, // bottom-right
    ]

    for (let i = 0; i < neighbours.length; i++) {
      const neighbour = new Vertex(neighbours[i])
      if (!groundLayer.getTileAt(neighbour.x, neighbour.y)) { continue }

      // If neighbor is not processed and not a wall
      if (isProcessed(processed, neighbour) || isWall(wallsLayer, neighbour)) { continue }

      const tempG = current.gScore + 1 // 1 will be replaced by node.cost later only

      // By default we expect, no shorter path will be discovered.
      let newPath = false

      // If neighbor was already accessed
      if (queue.contains(neighbour)) {
        // and the new path gScore is smaller then what was found previously
        if (tempG < neighbour.gScore) {
          /**
             gScore is the actual "cost" from beginning to end
           */
          neighbour.gScore = tempG
	  /**
	     fScore: Assumed cost, by the current heuristic,
	     to get from neighbor cell to target cell
	     Current priority queue implmentation expects fScore calculated
	     before get added to the queue since it order items based on it
           */
          neighbour.fScore = neighbour.gScore + h(neighbour, target)

          // Indicates that parent cell for neighbor needs to be changed to
	  // current node eg: path will be updated, a shorter route was found
          newPath = true
        }
      } else {
        // If node is not in queue, it should be added
        neighbour.gScore = tempG
	neighbour.fScore = neighbour.gScore + h(neighbour, target)
        newPath = true
        queue.add(neighbour)
      }

      if (newPath) {
        neighbour.parent = current
      }

      if (!(neighbour.x === target.x && neighbour.y === target.y)) {
	neighbour.mark(groundLayer, 0x00FF00)
      }
    }

    await waitForSeconds(0.01)
  }
  console.log('No Solution')
  return path
}

export default generatePath
