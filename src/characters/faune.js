"use strict"

import Phaser, { HEADLESS } from 'phaser'
import { sceneEvents } from '../events/EventCenter'
import Chest from '../items/chest'

const HealtState = {
    IDLE: 'IDLE',
    DAMAGE: 'DAMAGE',
    DEAD: 'DEAD'
}

/**
   Represent a lizard enemy class.

   @class
   @augments Phaser.Gameobjects.Image

   @param {Phaser.Scene} scene
   @param {number} x
   @param {number} y
   @param {string} texture
   @param {string|number} frame
 */
function Faune(scene, x, y, texture, frame) {
    Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, texture, frame)

    /**
       The x coordinate of the Lizard
       @type {number}
     */
    this.x = x

    /**
       The y coordinate of the Lizard
       @type {number}
     */
    this.y = y

    /**
       @member {Phaser.Time.TimerEvent }
     */
    this.moveEvent

    this.healthState = HealtState.IDLE
    /** @type {number} */
    this.damageTime = 0
    /** @type {number} */
    this._health = 3
    /** @type {number} */
    this._coins = 0

    /** @type Chest */
    this.activeChest

    this.knives

    /** @type Phaser.Math.Vector2 */
    this.movePath
    /** @type Phaser.Math.Vector2 */
    this.moveToTarget

    this.anims.play("faune-idle-down")
}
Faune.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
Faune.prototype.constructor = Faune


/**
   @param {Phaser.Math.Vector2} dir - Direction into the player should snap from its current location
 */
Faune.prototype.handleDamage = function(dir) {
    if (this._health <= 0) { return }
    if (this.healthState === HealtState.DAMAGE) { return }

    --this._health

    if (this._health <= 0) {
        // TODO: die
        this.healthState = HealtState.DEAD
        this.anims.play('faune-faint')
        this.setVelocity(0, 0)
    } else {
        this.setVelocity(dir.x, dir.y)
        this.setTint(0xff0000)
        this.healthState = HealtState.DAMAGE
        this.damageTime = 0
    }
}

Faune.prototype.health = function() {
    return this._health
}

Faune.prototype.preUpdate = function(time, delta) {
    this.__proto__.__proto__.preUpdate.call(this, time, delta)

    switch (this.healthState) {
        case HealtState.IDLE:
            break
        case HealtState.DAMAGE:
            this.damageTime += delta
            if (this.damageTime >= 250) {
                this.healthState = HealtState.IDLE
                this.setTint(0xfffffff)
                this.damageTime = 0
            }
            break
    }
}

Faune.prototype.update = function(cursors) {
    const speed = 100
    let moveLeft, moveRight, moveUp, moveDown
    let dx, dy

    if (this.healthState === HealtState.DAMAGE || this.healthState === HealtState.DEAD) { return }
    if (!cursors) { return }

    // Point and click navigation
    if (this.moveToTarget) {
        dx = this.moveToTarget.x - this.x
        dy = this.moveToTarget.y - this.y
        if (Math.abs(dx) < 5) {
            dx = 0
        }
        if (Math.abs(dy) < 5) {
            dy = 0
        }
        if (dx == 0 && dy == 0) {
            if (this.movePath.length > 0) {
                this.moveTo(this.movePath.shift())
                return
            }
            this.moveToTarget = undefined
        }
        // this logic is the same except we determine
        // if a key is down based on dx and dy
        moveLeft = dx < 0
        moveRight = dx > 0
        moveUp = dy < 0
        moveDown = dy > 0
    }

    // Keyboard based navigation
    if (Phaser.Input.Keyboard.JustDown(cursors.space)) {
        if (this.activeChest) {
            /** @type {number} */
            const coins = this.activeChest.coins
            this.activeChest.open()
            this._coins += coins
            sceneEvents.emit('player-coins-changed', this._coins)
        } else {
            throwKnife(this)
        }
        return
    }

    if (!this.moveToTarget) {
        moveLeft = cursors.left?.isDown
        moveRight = cursors.right?.isDown
        moveUp = cursors.up?.isDown
        moveDown = cursors.down?.isDown
    }

    if (moveLeft) {
        this.anims.play('faune-run-side', true)
        this.setVelocity(-speed, 0)
        this.scaleX = -1
        this.body.offset.x = 24
    } else if (moveRight) {
        this.anims.play('faune-run-side', true)
        this.setVelocity(speed, 0)
        this.scaleX = 1
        this.body.offset.x = 8
    } else if (moveUp) {
        this.anims.play('faune-run-up', true)
        this.setVelocity(0, -speed)
    } else if (moveDown) {
        this.anims.play('faune-run-down', true)
        this.setVelocity(0, speed)
    } else {
        const parts = this.anims.currentAnim.key.split('-')
        parts[1] = 'idle'
        this.play(parts.join('-'))
        this.setVelocity(0, 0)
    }

    if ([moveLeft, moveRight, moveUp, moveDown].indexOf(true) > -1) {
        this.activeChest = undefined
    }
}

Faune.prototype.setKnives = function(knives) {
    this.knives = knives
}

Faune.prototype.setChest = function(chest) {
    this.activeChest = chest
}

/**
   @param {Array<Phaser.Math.Vector2>} path - The path the faune should walks in
 */
Faune.prototype.moveAlong = function(path) {
    if (!path || path.length <= 0) {
        return
    }
    this.movePath = path
    this.moveTo(this.movePath.shift())
}

/**
   @param {Phaser.Math.Vector2} path - The next cell of the path the faune should reach
 */
Faune.prototype.moveTo = function(target) {
    // console.log("moveTo target", target, "faune x, y", this.x, this.y)
    this.moveToTarget = target
}

function throwKnife(faune) {
    if (!faune.knives) { return }

    const knife = faune.knives.get(faune.x, faune.y, 'knife')
    if (!knife) { return }

    const parts = faune.anims.currentAnim.key.split('-')
    const direction = parts[2]

    const vec = new Phaser.Math.Vector2(0, 0)
    switch (direction) {
        case 'up':
            vec.y = -1
            break
        case 'down':
            vec.y = 1
            break
        default:
        case 'side':
            if (faune.scaleX < 0) {
                vec.x = -1
            } else {
                vec.x = 1
            }
            break
    }
    const angle = vec.angle()

    knife.setActive(true)
    knife.setVisible(true)

    knife.setRotation(angle)
    knife.x += vec.x * 16
    knife.y += vec.y * 16
    knife.setVelocity(vec.x * 300, vec.y * 300)
}

/**
   Used in game.js at
     this.faune = this.add.faune
   it takes Faune off of the GameobjectFactory.registry
*/
Phaser.GameObjects.GameObjectFactory.register('faune', function(x, y, texture, frame) {
    var sprite = new Faune(this.scene, x, y, texture, frame);

    this.displayList.add(sprite);
    this.updateList.add(sprite);

    this.scene.physics.world.enableBody(sprite, Phaser.Physics.Arcade.DYNAMIC_BODY);

    sprite.body.setSize(sprite.width * 0.5, sprite.height * 0.8)
    return sprite
})

export default Faune
