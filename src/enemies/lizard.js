"use strict"

import Phaser from 'phaser'

const Direction = {
    UP: 'UP',
    DOWN: 'DOWN',
    LEFT: 'LEFT',
    RIGHT: 'RIGHT'
}

/**
   Represent a lizard enemy class.

   @class
   @augments Phaser.Gameobjects.Image

   @param {Phaser.Scene} scene
   @param {number} x
   @param {number} y
   @param {string} texture
   @param {string|number} frame
 */
function Lizard(scene, x, y, texture, frame) {
    Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, texture, frame)

    /**
       The x coordinate of the Lizard
       @member {number}
     */
    this.x = x

    /**
       The y coordinate of the Lizard
       @member {number}
     */
    this.y = y

    /**
       One of the keys from Direction object
       @member {string}
     */
    this.direction = Direction.RIGHT

    /**
       @member {Phaser.Time.TimerEvent }
    */
    this.moveEvent

    this.anims.play('lizard-idle')

    scene.physics.world.on(Phaser.Physics.Arcade.Events.TILE_COLLIDE, handleTileCollision, this)

    this.moveEvent = scene.time.addEvent({
        delay: 2000,
        callback: () => {
            this.direction = randomDirection(this.direction)
        },
        loop: true
    })
}

Lizard.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
Lizard.prototype.constructor = Lizard

/**
   @this Phaser.Physics.Arcade.Sprite.prototype
   @params {number} time
   @params {number} delta
 */
Lizard.prototype.preUpdate = function(time, delta) {
    this.__proto__.__proto__.preUpdate.call(this, time, delta)

    const speed = 50

    switch (this.direction) {
        case Direction.UP:
            this.setVelocity(0, -speed)
            break
        case Direction.DOWN:
            this.setVelocity(0, speed)
            break
        case Direction.RIGHT:
            this.setVelocity(speed, 0)
            break
        case Direction.LEFT:
            this.setVelocity(-speed, 0)
            break
    }
}

/**
   @param {bool} fromScene
*/
Lizard.prototype.destroy = function(fromScene) {
    this.moveEvent.destroy()

    // Inverse super logic for destroy we do our cleanup work first
    // to make sure parent cleanup don't destroy something we need
    // beforhand. Oppisite logic of update or create methods, where
    // we let parent first do its job and then addjust the result
    // according our special needs.
    this.__proto__.__proto__.destroy.call(this)
}

/**
   @memberof Lizard
   @this {Phaser.Arcade.Sprite}
   @params {Phaser.Gameobjects.GameObject} go
   @params {Phaser.Tilemaps.Tile} tile
 */
function handleTileCollision(go, tile) {
    if (go === this) {
        this.direction = randomDirection(this.direction)
    }
}

function randomDirection(excludedDirection) {
    const keys = Object.keys(Direction)
    let newDirection = Direction[keys[Phaser.Math.Between(0, keys.length - 1)]]
    let counter = 1

    while (newDirection === excludedDirection) {
        newDirection = Direction[keys[Phaser.Math.Between(0, keys.length - 1)]]
        if (counter > 10) { break }
        counter++
    }

    return newDirection
}

export default Lizard
