"use strict"

import Phaser from "phaser"

/**
   Preloading all assets necessary for the game
   @class
 */
function PreloadScene() {
    Phaser.Scene.call(this, this.constructor.name)
}
PreloadScene.prototype = Object.create(Phaser.Scene.prototype)
PreloadScene.prototype.constructor = PreloadScene
/**
   @this Phaser.Scene
 */
PreloadScene.prototype.preload = function() {
    this.load.image('tiles', 'tiles/dungeon_tiles_extruded.png')
    this.load.tilemapTiledJSON('dungeon', 'tiles/dungeon-01.json')
    this.load.atlas('faune', 'character/fauna.png', 'character/fauna.json')
    this.load.atlas('lizard', 'enemies/lizard.png', 'enemies/lizard.json')
    this.load.atlas('chest', 'items/treasure.png', 'items/treasure.json')

    this.load.image('ui-heart-empty', 'ui/ui_heart_empty.png')
    this.load.image('ui-heart-full', 'ui/ui_heart_full.png')
    this.load.image('knife', 'weapons/weapon_knife.png')
}

PreloadScene.prototype.create = function() {
    this.scene.start('game')
}

export default PreloadScene
