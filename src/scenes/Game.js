"use strict"

import Phaser from 'phaser'
import { createCharacterAnims } from '../anims/character_anims'
import { createChestAnims } from '../anims/chest_anims'
import { createLizardAnims } from '../anims/enemy_anims'
import Faune from '../characters/faune'
import Lizard from '../enemies/lizard'
import { sceneEvents } from '../events/EventCenter'
// import findPath from '../utils/breath_first'
import Chest from '../items/chest'
import { debugDraw } from '../utils/debug'
import generatePath from '../utils/a_star'

function Game() {
  Phaser.Scene.call(this, 'game')
}
Game.prototype = Object.create(Phaser.Scene.prototype)
Game.prototype.constructor = Game

Game.prototype.preload = function() {
  this.cursors = this.input.keyboard.createCursorKeys()
}

/**
   @this Phaser.Scene

 */
Game.prototype.create = function() {
  this.scene.run('game-ui')
  createCharacterAnims(this.anims)
  createLizardAnims(this.anims)
  createChestAnims(this.anims)

  //this.add.image(0, 0, 'tiles')
  const map = this.make.tilemap({ key: 'dungeon' })
  const tileset = map.addTilesetImage('dungeon', 'tiles', 16, 16, 1, 2)
  const groundLayer = map.createLayer('Ground', tileset)

  const wallsLayer = map.createLayer('Walls', tileset)
  wallsLayer.setCollisionByProperty({ collides: true })
  debugDraw(wallsLayer, this)

  const chests = this.physics.add.staticGroup({
    classType: Chest
  })
  const chestLayer = map.getObjectLayer('Chest')
  chestLayer.objects.forEach((chestObject) => {
    chests.get(chestObject.x + chestObject.width * 0.5, chestObject.y - chestObject.height * 0.5, 'chest')
  })

  /*  const chest = this.add.sprite(64, 64, 'chest', 'chest_empty_open_anim_f0')
   * this.time.delayedCall(1000, function() {
   *     chest.play('chest-open')
   * })
   */
  this.knives = this.physics.add.group({
    classType: Phaser.Physics.Arcade.Image,
    maxSize: 3
  })

  // Creating Faune through GameObjectFactory defined in characters/faune.js
  // but globally for Phaser.Gameobjects, eg its a top level global reference
  // in this game and can be accessed by 'faune' key
  // https://newdocs.phaser.io/docs/3.60.0/Phaser.GameObjects.GameObjectFactory#register
  /**
     @type {Faune}
   */
  this.faune = this.add.faune(128, 128, 'faune')
  this.faune.setKnives(this.knives)
  this.cameras.main.startFollow(this.faune, true)

  const lizardsLayer = map.getObjectLayer('Lizard')
  this.lizards = this.physics.add.group({
    classType: Lizard,
    createCallback: (go) => {
      go.body.onCollide = true
    }
  })

  lizardsLayer.objects.forEach((lizard) => {
    //this.lizards.get(lizard.x + lizard.width * 0.5, lizard.y + lizard.height * 0.5, 'lizard')
  })


  this.physics.add.collider(this.faune, wallsLayer)
  this.physics.add.collider(this.faune, chests, handlePlayerChestCollistion, undefined, this)
  this.physics.add.collider(this.lizards, wallsLayer)
  this.physics.add.collider(this.knives, wallsLayer, handleKnifeWallCollision, undefined, this)
  this.physics.add.collider(this.knives, this.lizards, handleKnifeLizardCollision, undefined, this)
  this.playerLizardCollider = this.physics.add.collider(this.lizards, this.faune, handleFauneLizardCollision, undefined, this)

  this.input.on(Phaser.Input.Events.POINTER_UP, async (pointer) => {
    const { worldX, worldY } = pointer
    const startVec = groundLayer.worldToTileXY(this.faune.x, this.faune.y)
    const targetVec = groundLayer.worldToTileXY(worldX, worldY)
    // In tile coordinates
    console.log("Click start", startVec, "target", targetVec)
    // const path = await findPath(startVec, targetVec, { groundLayer, wallsLayer, chestLayer })
    const path = await generatePath(startVec, targetVec, { groundLayer, wallsLayer, chestLayer })
    console.log("Move along, we need to wait for generatePath to populate route")
    this.faune.moveAlong(path)
  })

  this.events.once(Phaser.Scenes.Events.SHUTDOWN, () => {
    this.input.off(Phaser.Input.Events.POINTER_OUT)
  })
}

Game.prototype.update = function(time, delta) {
  if (this.faune) {
    this.faune.update(this.cursors)
  }
}

/**
   @this Game
   @param {Faune} faune
   @param {Chest} chest
 */
function handlePlayerChestCollistion(faune, chest) {
  // console.dir(faun)
  // console.dir(chest)
  this.faune.setChest(chest)
}

/**
   @this Game
   @param {Faune} faune
   @param {Lizard} lizard
 */
function handleFauneLizardCollision(faune, lizard) {
  //console.dir(go1)
  //console.dir(go2)

  const dx = faune.x - lizard.x
  const dy = faune.y - lizard.y

  const dir = new Phaser.Math.Vector2(dx, dy).normalize().scale(200)

  faune.handleDamage(dir)
  sceneEvents.emit('player-health-changed', faune.health())

  if (this.faune.health() == 0) {
    this.playerLizardCollider.destroy()
  }
}

/**
   @this Game
 */
function handleKnifeLizardCollision(knife, lizard) {
  this.knives.killAndHide(knife)
  this.lizards.killAndHide(lizard)
}

// Presenter SuperTommi from ourcade says the order is not deterministic
// sometimes so he alway look up which obj is which
/**
   @this Game
 */
function handleKnifeWallCollision(obj1, obj2) {
  // console.dir(obj1)
  // console.dir(obj2)
  const knife = obj1
  const wall = obj2

  this.knives.killAndHide(knife)
}

export default Game
