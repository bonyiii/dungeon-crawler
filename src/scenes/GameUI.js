"use strict"

import Phaser from 'phaser'
import { sceneEvents } from '../events/EventCenter'

/**

@class
@augments Phaser.Scene
*/
function GameUI() {
    Phaser.Scene.call(this, 'game-ui')
    this.hearts
}
GameUI.prototype = Object.create(Phaser.Scene.prototype)
GameUI.prototype.constructor = GameUI

GameUI.prototype.create = function() {
    this.add.image(6, 26, 'chest', 'coin_anim_f0.png')
    const coinsLabel = this.add.text(12, 20, '0', {
        fontSize: '1.5em'
    })

    sceneEvents.on('player-coins-changed', (coins) => {
        coinsLabel.text = coins.toLocaleString()
    })
    this.hearts = this.add.group({
        classType: Phaser.GameObjects.Image
    })

    this.hearts.createMultiple({
        key: 'ui-heart-full',
        setXY: {
            x: 10,
            y: 10,
            stepX: 16
        },
        quantity: 3
    })

    sceneEvents.on('player-health-changed', handlePlayerHealthChange, this)
    this.events.once(Phaser.Scenes.Events.SHUTDOWN, () => {
        sceneEvents.off('player-health-changed', handlePlayerHealthChange)
        sceneEvents.off('player-coins-changed ')
    })
}

function handlePlayerHealthChange(fauneHealth) {
    /**
       @param {Phaser.GameObjects.Image} heart - Heart icon, indication player's life
       @param {number} idx - Index of the current heart
     */
    this.hearts.children.each((heart, idx) => {
        if (idx < fauneHealth) {
            heart.setTexture('ui-heart-full')
        } else {
            heart.setTexture('ui-heart-empty')
        }
    })
}

export default GameUI
